#include "FBullCowGame.h"
#include <iostream>
#include <string>
#include <map>
#include <fstream>
#include <random>


FBullCowGame::FBullCowGame() { Reset(); }

int FBullCowGame::GetMaxTries() const
{
	std::map<const std::string, int> WordLengthToMaxTries{HiddenWord.length(), (HiddenWord.length() - 1)};
	return WordLengthToMaxTries[HiddenWord.length()];
}

int FBullCowGame::GetWord() const
{
	return 0;
}

int FBullCowGame::GetCurrentTry() const
{
	return CurrentTry; // TODO: Update the Current Try
}

int FBullCowGame::GetHiddenWordLength() const
{
	return HiddenWord.length();
}

bool FBullCowGame::IsGameWon() const
{
	return bGameIsWon;
}


EGuessStatus FBullCowGame::IsGuessValid(std::string Guess) const
{
	if (Guess == "GetHiddenWord")
	{
		std::cout << HiddenWord << "\n";
		return EGuessStatus::DEV_COMMAND;
	}
	if (!IsIsogram(Guess)) // TODO: Write Function
	{
		return EGuessStatus::NOT_ISOGRAM;
	}
	else if (!isLowercase(Guess)) // TODO: Write Function
	{
		return EGuessStatus::NOT_LOWERCASE;
	}
	else if (Guess.length() != GetHiddenWordLength())
	{
		return EGuessStatus::WRONG_LENGTH;
	}
	else
	{
		return EGuessStatus::OK;
	}
}

void FBullCowGame::Reset()
{
	const std::string HIDDEN_WORD = "planet";

	HiddenWord = HIDDEN_WORD;
	CurrentTry = 1;
	bGameIsWon = false;
	
	return;
}

// Recieves a Valid guess and returns the Bulls and Cows and increments the turn
FBullCowCount FBullCowGame::SubmitValidGuess(std::string Guess)
{
	CurrentTry++;

	FBullCowCount BullCowCount;
	int WordLength = HiddenWord.length();
	// Loop through all letters in the guess
	for (int i = 0; i < WordLength; i++) 
	{
		for (int j = 0; j < WordLength; j++) // Compare the letters aganist the hidden word
		{
			if (Guess[j] == HiddenWord[i]) 
			{
				if (i == j)
				{
					BullCowCount.Bulls++; 
				}
				else
				{
					BullCowCount.Cows++; 
				}
			}
		}	
	}
	if (BullCowCount.Bulls == WordLength)
	{
		bGameIsWon = true;
	}
	else
	{
		bGameIsWon = false;
	}


	return BullCowCount;
}

bool FBullCowGame::IsIsogram(std::string Word) const
{
	if (Word.length() <= 1)
	{
		return true;
	}

	std::map<char, bool> LetterSeen;

	for (auto Letter : Word)
	{
		Letter = tolower(Letter);

		if (LetterSeen[Letter])
		{
			return false;
		}
		else
		{
			LetterSeen[Letter] = true;
		}

	}

	return true;
}

bool FBullCowGame::isLowercase(std::string Word) const
{

	for (auto Letter : Word)
	{
		if (!islower(Letter))
		{
			return false;
		}
	}

	return true;
}
