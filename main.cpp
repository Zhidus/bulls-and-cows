/*
This is the console executable that makes use of FBullCowGame.cpp. This acts as the view in a MVC pattern and is 
responcable for all user interaction. For game logic see the FBullCowGame class.
*/


#include <iostream>
#include <string>
#include "FBullCowGame.h"

void PrintIntroduction();
void PlayGame();
void PrintGameSummary();
std::string GetValidGuess();
bool ShouldPlayAgain();

	FBullCowGame BCGame; // Instantiate the new game
	int NUMBER_OF_TURNS = BCGame.GetMaxTries();

// Entry Point of the Bulls and Cows Game
int main() 
{
	bool bPlayAgain = false;
	do 
	{
		PrintIntroduction();
		PlayGame();
		bPlayAgain = ShouldPlayAgain();
	} while (bPlayAgain);
	return 0; // Exit the Command Prompt
}

void PrintIntroduction() 
{
	// Text before the game
	std::cout << "Welcome to Bulls and Cows\n";
	std::cout << "Can you guess the " << BCGame.GetHiddenWordLength();
	std::cout << " letter isogram I am thinking of?" << std::endl;
	std::cout << std::endl;
	return;
}

void PlayGame()
{
	BCGame.Reset();
	int MaxTries = BCGame.GetMaxTries();

	while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= MaxTries)
	{
		std::string Guess = GetValidGuess();

		// Submit Guess and Recieve Bulls and Cows
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(Guess);

		std::cout << "Bulls = " << BullCowCount.Bulls;
		std::cout << " | Cows = " << BullCowCount.Cows << "\n\n";
	}

	PrintGameSummary();
	return;
}

void PrintGameSummary()
{
	if (BCGame.IsGameWon() == true)
	{
		std::cout << "You have won! \n";
	}
	else if (BCGame.IsGameWon() == false)
	{
		std::cout << "You have lost! Better luck next time! \n";
	}
}

// Loops until the player gets a valid guess
std::string GetValidGuess()
{
	std::string Guess = "";
	EGuessStatus Status = EGuessStatus::INVALID_STATUS;
	do {
		// Have the player guess a word
		int CurrentTry = BCGame.GetCurrentTry();
		std::cout << "Try " << CurrentTry << "/" << NUMBER_OF_TURNS << std::endl;
		std::cout << "Please type your Guess:";
		getline(std::cin, Guess);

		Status = BCGame.IsGuessValid(Guess);
		switch (Status)
		{
		case EGuessStatus::WRONG_LENGTH:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n";
			break;
		case EGuessStatus::NOT_ISOGRAM:
			std::cout << "Please enter a word without repeating letters.\n";
			break;
		case EGuessStatus::NOT_LOWERCASE:
			std::cout << "Please make your guess all lowercase\n";
			break;
		case EGuessStatus::DEV_COMMAND:
			std::cout << "You have used a developer command\n";
			break;
		default:
			break;
		}
		std::cout << std::endl;
	} while (Status != EGuessStatus::OK); // Keep looping until player inserts a valid input
	return Guess;
}

bool ShouldPlayAgain()
{
	std::cout << "Do you wish to try again with the same word? [y/n]";
	std::string Response = "";
	getline(std::cin, Response);
	return (Response[0] == 'y') || (Response[0] == 'Y');
}

