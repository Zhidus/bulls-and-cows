#pragma once
#include <string>

struct FBullCowCount 
{
	int Bulls = 0;
	int Cows = 0;
};

enum class EGuessStatus 
{
	INVALID_STATUS,
	OK,
	NOT_ISOGRAM,
	NOT_LOWERCASE,
	WRONG_LENGTH,
	USES_NUMBERS,
	DEV_COMMAND
};

class FBullCowGame 
{
public:
	FBullCowGame();

	int GetMaxTries() const; 
	int GetWord() const;
	int GetCurrentTry() const;
	int GetHiddenWordLength() const;
	bool IsGameWon() const;
	
	EGuessStatus IsGuessValid(std::string) const;

	void Reset(); // TODO: Make a more rich return value
	FBullCowCount SubmitValidGuess(std::string);


	// TODO: Provide a method for counting the number of bulls and cows and increasing the CurrentTry

private:
	// See Constructor in FBullCowGame.cpp for initialisation
	int CurrentTry;
	bool bGameIsWon;
	std::string HiddenWord;

	bool IsIsogram(std::string) const;
	bool isLowercase(std::string) const;

};